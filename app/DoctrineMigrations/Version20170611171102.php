<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170611171102 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_cart DROP FOREIGN KEY FK_22F9D3C74584665A');
        $this->addSql('ALTER TABLE group_cart DROP FOREIGN KEY FK_22F9D3C76FFE5F71');
        $this->addSql('ALTER TABLE group_cart CHANGE product_id product_id INT NOT NULL, CHANGE user_has_group_id user_has_group_id INT NOT NULL');
        $this->addSql('ALTER TABLE group_cart ADD CONSTRAINT FK_22F9D3C74584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_cart ADD CONSTRAINT FK_22F9D3C76FFE5F71 FOREIGN KEY (user_has_group_id) REFERENCES user_has_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0A76ED395');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0D7707B45');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0EBF23851');
        $this->addSql('ALTER TABLE user_order CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0D7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0EBF23851 FOREIGN KEY (delivery_address_id) REFERENCES delivery_address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_has_group DROP FOREIGN KEY FK_96A9D99FA76ED395');
        $this->addSql('ALTER TABLE user_has_group CHANGE user_group_id user_group_id INT NOT NULL, CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_has_group ADD CONSTRAINT FK_96A9D99FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_order_has_product DROP FOREIGN KEY FK_5716A8DB4584665A');
        $this->addSql('ALTER TABLE user_order_has_product DROP FOREIGN KEY FK_5716A8DB6D128938');
        $this->addSql('ALTER TABLE user_order_has_product CHANGE product_id product_id INT NOT NULL, CHANGE user_order_id user_order_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_order_has_product ADD CONSTRAINT FK_5716A8DB4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_order_has_product ADD CONSTRAINT FK_5716A8DB6D128938 FOREIGN KEY (user_order_id) REFERENCES user_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_group DROP FOREIGN KEY FK_8F02BF9D7E3C61F9');
        $this->addSql('ALTER TABLE user_group CHANGE owner_id owner_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_group ADD CONSTRAINT FK_8F02BF9D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_address DROP FOREIGN KEY FK_750D05FA76ED395');
        $this->addSql('ALTER TABLE delivery_address CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE delivery_address ADD CONSTRAINT FK_750D05FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE join_request DROP FOREIGN KEY FK_E932E4FFA76ED395');
        $this->addSql('ALTER TABLE join_request CHANGE user_group_id user_group_id INT NOT NULL, CHANGE user_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE join_request ADD CONSTRAINT FK_E932E4FFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FFD7707B45');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FFEBF23851');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FFD7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FFEBF23851 FOREIGN KEY (delivery_address_id) REFERENCES delivery_address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC4584665A');
        $this->addSql('ALTER TABLE group_order_has_product CHANGE product_id product_id INT NOT NULL, CHANGE user_has_group_id user_has_group_id INT NOT NULL, CHANGE group_order_id group_order_id INT NOT NULL');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery_address DROP FOREIGN KEY FK_750D05FA76ED395');
        $this->addSql('ALTER TABLE delivery_address CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery_address ADD CONSTRAINT FK_750D05FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE group_cart DROP FOREIGN KEY FK_22F9D3C76FFE5F71');
        $this->addSql('ALTER TABLE group_cart DROP FOREIGN KEY FK_22F9D3C74584665A');
        $this->addSql('ALTER TABLE group_cart CHANGE user_has_group_id user_has_group_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_cart ADD CONSTRAINT FK_22F9D3C76FFE5F71 FOREIGN KEY (user_has_group_id) REFERENCES user_has_group (id)');
        $this->addSql('ALTER TABLE group_cart ADD CONSTRAINT FK_22F9D3C74584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FFD7707B45');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FFEBF23851');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FFD7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FFEBF23851 FOREIGN KEY (delivery_address_id) REFERENCES delivery_address (id)');
        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC4584665A');
        $this->addSql('ALTER TABLE group_order_has_product CHANGE user_has_group_id user_has_group_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL, CHANGE group_order_id group_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE join_request DROP FOREIGN KEY FK_E932E4FFA76ED395');
        $this->addSql('ALTER TABLE join_request CHANGE user_id user_id INT DEFAULT NULL, CHANGE user_group_id user_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE join_request ADD CONSTRAINT FK_E932E4FFA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_group DROP FOREIGN KEY FK_8F02BF9D7E3C61F9');
        $this->addSql('ALTER TABLE user_group CHANGE owner_id owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group ADD CONSTRAINT FK_8F02BF9D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_group DROP FOREIGN KEY FK_96A9D99FA76ED395');
        $this->addSql('ALTER TABLE user_has_group CHANGE user_id user_id INT DEFAULT NULL, CHANGE user_group_id user_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_has_group ADD CONSTRAINT FK_96A9D99FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0A76ED395');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0EBF23851');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0D7707B45');
        $this->addSql('ALTER TABLE user_order CHANGE user_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0EBF23851 FOREIGN KEY (delivery_address_id) REFERENCES delivery_address (id)');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0D7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE user_order_has_product DROP FOREIGN KEY FK_5716A8DB6D128938');
        $this->addSql('ALTER TABLE user_order_has_product DROP FOREIGN KEY FK_5716A8DB4584665A');
        $this->addSql('ALTER TABLE user_order_has_product CHANGE user_order_id user_order_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_order_has_product ADD CONSTRAINT FK_5716A8DB6D128938 FOREIGN KEY (user_order_id) REFERENCES user_order (id)');
        $this->addSql('ALTER TABLE user_order_has_product ADD CONSTRAINT FK_5716A8DB4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }
}
