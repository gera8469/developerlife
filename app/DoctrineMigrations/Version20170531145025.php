<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170531145025 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_cart ADD user_has_group_id INT DEFAULT NULL, ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_cart ADD CONSTRAINT FK_22F9D3C76FFE5F71 FOREIGN KEY (user_has_group_id) REFERENCES user_has_group (id)');
        $this->addSql('ALTER TABLE group_cart ADD CONSTRAINT FK_22F9D3C74584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_22F9D3C76FFE5F71 ON group_cart (user_has_group_id)');
        $this->addSql('CREATE INDEX IDX_22F9D3C74584665A ON group_cart (product_id)');
        $this->addSql('ALTER TABLE user_order ADD user_id INT DEFAULT NULL, ADD delivery_address_id INT DEFAULT NULL, ADD order_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0EBF23851 FOREIGN KEY (delivery_address_id) REFERENCES delivery_address (id)');
        $this->addSql('ALTER TABLE user_order ADD CONSTRAINT FK_17EB68C0D7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id)');
        $this->addSql('CREATE INDEX IDX_17EB68C0A76ED395 ON user_order (user_id)');
        $this->addSql('CREATE INDEX IDX_17EB68C0EBF23851 ON user_order (delivery_address_id)');
        $this->addSql('CREATE INDEX IDX_17EB68C0D7707B45 ON user_order (order_status_id)');
        $this->addSql('ALTER TABLE user_has_group ADD user_id INT DEFAULT NULL, ADD user_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_has_group ADD CONSTRAINT FK_96A9D99FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_group ADD CONSTRAINT FK_96A9D99F1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('CREATE INDEX IDX_96A9D99FA76ED395 ON user_has_group (user_id)');
        $this->addSql('CREATE INDEX IDX_96A9D99F1ED93D47 ON user_has_group (user_group_id)');
        $this->addSql('ALTER TABLE user_order_has_product ADD user_order_id INT DEFAULT NULL, ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_order_has_product ADD CONSTRAINT FK_5716A8DB6D128938 FOREIGN KEY (user_order_id) REFERENCES user_order (id)');
        $this->addSql('ALTER TABLE user_order_has_product ADD CONSTRAINT FK_5716A8DB4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_5716A8DB6D128938 ON user_order_has_product (user_order_id)');
        $this->addSql('CREATE INDEX IDX_5716A8DB4584665A ON user_order_has_product (product_id)');
        $this->addSql('ALTER TABLE product ADD category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD12469DE2 ON product (category_id)');
        $this->addSql('ALTER TABLE user_group ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_group ADD CONSTRAINT FK_8F02BF9D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_8F02BF9D7E3C61F9 ON user_group (owner_id)');
        $this->addSql('ALTER TABLE delivery_address ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery_address ADD CONSTRAINT FK_750D05FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_750D05FA76ED395 ON delivery_address (user_id)');
        $this->addSql('ALTER TABLE group_order ADD order_status_id INT DEFAULT NULL, ADD user_group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FFD7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE group_order ADD CONSTRAINT FK_A505B8FF1ED93D47 FOREIGN KEY (user_group_id) REFERENCES user_group (id)');
        $this->addSql('CREATE INDEX IDX_A505B8FFD7707B45 ON group_order (order_status_id)');
        $this->addSql('CREATE INDEX IDX_A505B8FF1ED93D47 ON group_order (user_group_id)');
        $this->addSql('ALTER TABLE category ADD parent_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1796A8F92 FOREIGN KEY (parent_category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_64C19C1796A8F92 ON category (parent_category_id)');
        $this->addSql('ALTER TABLE group_order_has_product ADD user_has_group_id INT DEFAULT NULL, ADD product_id INT DEFAULT NULL, ADD group_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC6FFE5F71 FOREIGN KEY (user_has_group_id) REFERENCES user_has_group (id)');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE group_order_has_product ADD CONSTRAINT FK_714D3CCC84AF2147 FOREIGN KEY (group_order_id) REFERENCES group_order (id)');
        $this->addSql('CREATE INDEX IDX_714D3CCC6FFE5F71 ON group_order_has_product (user_has_group_id)');
        $this->addSql('CREATE INDEX IDX_714D3CCC4584665A ON group_order_has_product (product_id)');
        $this->addSql('CREATE INDEX IDX_714D3CCC84AF2147 ON group_order_has_product (group_order_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1796A8F92');
        $this->addSql('DROP INDEX IDX_64C19C1796A8F92 ON category');
        $this->addSql('ALTER TABLE category DROP parent_category_id');
        $this->addSql('ALTER TABLE delivery_address DROP FOREIGN KEY FK_750D05FA76ED395');
        $this->addSql('DROP INDEX IDX_750D05FA76ED395 ON delivery_address');
        $this->addSql('ALTER TABLE delivery_address DROP user_id');
        $this->addSql('ALTER TABLE group_cart DROP FOREIGN KEY FK_22F9D3C76FFE5F71');
        $this->addSql('ALTER TABLE group_cart DROP FOREIGN KEY FK_22F9D3C74584665A');
        $this->addSql('DROP INDEX IDX_22F9D3C76FFE5F71 ON group_cart');
        $this->addSql('DROP INDEX IDX_22F9D3C74584665A ON group_cart');
        $this->addSql('ALTER TABLE group_cart DROP user_has_group_id, DROP product_id');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FFD7707B45');
        $this->addSql('ALTER TABLE group_order DROP FOREIGN KEY FK_A505B8FF1ED93D47');
        $this->addSql('DROP INDEX IDX_A505B8FFD7707B45 ON group_order');
        $this->addSql('DROP INDEX IDX_A505B8FF1ED93D47 ON group_order');
        $this->addSql('ALTER TABLE group_order DROP order_status_id, DROP user_group_id');
        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC6FFE5F71');
        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC4584665A');
        $this->addSql('ALTER TABLE group_order_has_product DROP FOREIGN KEY FK_714D3CCC84AF2147');
        $this->addSql('DROP INDEX IDX_714D3CCC6FFE5F71 ON group_order_has_product');
        $this->addSql('DROP INDEX IDX_714D3CCC4584665A ON group_order_has_product');
        $this->addSql('DROP INDEX IDX_714D3CCC84AF2147 ON group_order_has_product');
        $this->addSql('ALTER TABLE group_order_has_product DROP user_has_group_id, DROP product_id, DROP group_order_id');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD12469DE2');
        $this->addSql('DROP INDEX IDX_D34A04AD12469DE2 ON product');
        $this->addSql('ALTER TABLE product DROP category_id');
        $this->addSql('ALTER TABLE user_group DROP FOREIGN KEY FK_8F02BF9D7E3C61F9');
        $this->addSql('DROP INDEX IDX_8F02BF9D7E3C61F9 ON user_group');
        $this->addSql('ALTER TABLE user_group DROP owner_id');
        $this->addSql('ALTER TABLE user_has_group DROP FOREIGN KEY FK_96A9D99FA76ED395');
        $this->addSql('ALTER TABLE user_has_group DROP FOREIGN KEY FK_96A9D99F1ED93D47');
        $this->addSql('DROP INDEX IDX_96A9D99FA76ED395 ON user_has_group');
        $this->addSql('DROP INDEX IDX_96A9D99F1ED93D47 ON user_has_group');
        $this->addSql('ALTER TABLE user_has_group DROP user_id, DROP user_group_id');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0A76ED395');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0EBF23851');
        $this->addSql('ALTER TABLE user_order DROP FOREIGN KEY FK_17EB68C0D7707B45');
        $this->addSql('DROP INDEX IDX_17EB68C0A76ED395 ON user_order');
        $this->addSql('DROP INDEX IDX_17EB68C0EBF23851 ON user_order');
        $this->addSql('DROP INDEX IDX_17EB68C0D7707B45 ON user_order');
        $this->addSql('ALTER TABLE user_order DROP user_id, DROP delivery_address_id, DROP order_status_id');
        $this->addSql('ALTER TABLE user_order_has_product DROP FOREIGN KEY FK_5716A8DB6D128938');
        $this->addSql('ALTER TABLE user_order_has_product DROP FOREIGN KEY FK_5716A8DB4584665A');
        $this->addSql('DROP INDEX IDX_5716A8DB6D128938 ON user_order_has_product');
        $this->addSql('DROP INDEX IDX_5716A8DB4584665A ON user_order_has_product');
        $this->addSql('ALTER TABLE user_order_has_product DROP user_order_id, DROP product_id');
    }
}
