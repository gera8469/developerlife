<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 01.06.17
 * Time: 20:28
 */

namespace DeveloperLifeBundle\Controller\Admin\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use DeveloperLifeBundle\Form\Admin\Shop\Shop\ProductType;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProductController
 *
 * Handle product
 *
 * @package DeveloperLifeBundle\Controller\Admin\Shop\Shop
 */
class ProductController extends Controller
{
    /**
     * Product list
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $repository = $this->getProductRepository();
        $query = $repository->createQueryBuilder('pr')
            ->select([
                'pr',
                'c'
            ])
            ->innerJoin('pr.category', 'c');
        $paginator = $this->get('knp_paginator');

        /* @var $pagination PaginationInterface */
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@DeveloperLife/admin/shop/product/indexProduct.html.twig',[
            'pagination' => $pagination,
        ]);
    }

    /**
     * New product
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product,[
            'action' => $this->generateUrl('admin_shop_product_new')
        ]);
        $form->handleRequest($request);

        /* @var $image UploadedFile */
        if ($form->isSubmitted() && $form->isValid()) {
            $image = $product->getUploadImage();

            if ($image) {
                $product->setImage($this->generateNewImagePath($image));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('admin_shop_product_show', [
                'id' => $product->getId(),
            ]);
        }

        return $this->render('@DeveloperLife/admin/shop/product/newProduct.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edit product
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $repository = $this->getProductRepository();
        /* @var $product Product*/
        $product = $repository->find($id);

        if (!$product) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(ProductType::class, $product,[
            'action' => $this->generateUrl('admin_shop_product_edit', [
                'id' => $product->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($uploadImage = $product->getUploadImage()) {
                $product->setImage($this->generateNewImagePath($product->getUploadImage()));
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('admin_shop_product_show', [
                'id' => $product->getId(),
            ]);
        }

        return $this->render('@DeveloperLife/admin/shop/product/newProduct.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show product
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {

        $repository = $this->getProductRepository();
        $product = $repository->find($id);

        if (!$product) {
            throw $this->createNotFoundException();
        }

        return $this->render('@DeveloperLife/admin/shop/product/showProduct.html.twig',[
            'product' => $product
        ]);
    }

    /**
     * Remove product
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($id)
    {
        $repository = $this->getProductRepository();
        $category = $repository->find($id);
        $em = $this->getDoctrine()->getManager();

        if (!$category) {
            throw $this->createNotFoundException();
        }

        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('admin_shop_product');
    }

    /**
     * Generate path for image file
     *
     * @param UploadedFile $image
     * @return string
     */
    private function generateNewImagePath(UploadedFile $image)
    {
        return $this->getParameter('image_config')['product']['relative_path'] .
            '/' . md5(uniqid()) . '.' . $image->getClientOriginalExtension();
    }

    /**
     * Return ProductRepository
     *
     * @return \DeveloperLifeBundle\Repository\Shop\Shop\ProductRepository
     */
    private function getProductRepository()
    {
        return $this->getDoctrine()->getRepository(Product::class);

    }
}