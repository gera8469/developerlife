<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.05.17
 * Time: 14:34
 */

namespace DeveloperLifeBundle\Controller\Admin\Security;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SecurityController
 *
 * @package DeveloperLifeBundle\Controller\Admin\Security
 */
class SecurityController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUserName = $authenticationUtils->getLastUsername();

        return $this->render('@DeveloperLife/admin/security/form/loginForm.html.twig', [
            'lastUserName' => $lastUserName,
            'error' => $error,
        ]);
    }
}