<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 31.05.17
 * Time: 13:31
 */

namespace DeveloperLifeBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MainController
 *
 * @package DeveloperLifeBundle\Controller\Admin
 */
class MainController extends Controller
{
    /**
     * Render admin panel index page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('@DeveloperLife/layouts/adminLayout.html.twig');
    }
}