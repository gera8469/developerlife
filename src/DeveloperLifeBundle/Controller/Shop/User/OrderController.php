<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.06.17
 * Time: 18:14
 */

namespace DeveloperLifeBundle\Controller\Shop\User;

use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder;
use DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct;
use DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup;
use DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress;
use DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder;
use DeveloperLifeBundle\Form\Shop\Group\Order\GroupOrderType;
use DeveloperLifeBundle\Form\Shop\User\Order\UserOrderType;
use DeveloperLifeBundle\Helper\Cart\Base\AbstractCartHandler;
use DeveloperLifeBundle\Helper\Cart\CartHandlerFactory;
use DeveloperLifeBundle\Repository\Shop\Group\Order\GroupOrderRepository;
use DeveloperLifeBundle\Repository\Shop\User\Order\UserOrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class OrderController
 *
 * Handle order
 *
 * @package DeveloperLifeBundle\Controller\Shop\User
 */
class OrderController extends Controller
{
    /**
     * Order config by cart alias. Contains params what need for order handling
     *
     * @var array
     */
    private $orderConfigByCartAlias = [
        'userCart' => [
            'formClass' => UserOrderType::class,
            'orderClass' => UserOrder::class,
            'orderType' => 'user',
        ],
        'groupCart' => [
            'formClass' => GroupOrderType::class,
            'orderClass' => GroupOrder::class,
            'orderType' => 'group',
        ],
    ];


    /**
     * Order list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {

        return $this->render('@DeveloperLife/user/order/indexOrder.html.twig', [
            'userOrders' => $this->get('doctrine')->getRepository($this->orderConfigByCartAlias['userCart']['orderClass'])
                ->findBy([
                'user' => $this->getUser(),
            ]),
            'groupOrders' => $this->get('doctrine')->getRepository($this->orderConfigByCartAlias['groupCart']['orderClass'])
                ->findOrderListByUser($this->getUser()),
        ]);
    }

    /**
     * Checkout order
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function checkoutAction(Request $request)
    {
        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];
        $orderConfig = $this->orderConfigByCartAlias[$cartAlias];

        $cart = CartHandlerFactory::getCartHandler(
            $cartAlias,
            $request->cookies->get($cartAlias),
            $this->get('service_container')
        );

        $doctrine = $this->getDoctrine();

        $addresses = $doctrine->getRepository(DeliveryAddress::class)->findBy([
            'user' => $this->getUser(),
        ]);

        if ($cart->isEmpty()) {

            return $this->redirectToRoute('shop_profile_cart');
        } elseif (!count($addresses)){

            return $this->redirectToRoute('shop_profile_address_new');
        }

        $orderClassName = $orderConfig['orderClass'];
        $order = new $orderClassName();

        $form = $this->createForm($orderConfig['formClass'], $order,[
            'action' => $this->generateUrl('shop_profile_order_checkout'),
            'logged_user' => $this->getUser(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cart->convertCartToOrder($order);

            $redirectResponse = $this->redirectToRoute('shop_profile_order_show',[
                'id' => $order->getId(),
                'orderType' => $this->orderConfigByCartAlias[$cartAlias]['orderType']
            ]);
            $redirectResponse->headers->clearCookie($cartAlias);

            return $redirectResponse;
        }

        return $this->render('@DeveloperLife/user/order/checkout.html.twig',[
            'form' => $form->createView(),
            'cartData' => $cart->getCartProducts(),
            'cartAlias' => $cartAlias,
        ]);
    }

    /**
     * Show order
     *
     * @throws NotFoundHttpException
     * @throws AccessDeniedHttpException
     * @param integer $id
     * @param string $orderType Order type alias (user|group)
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id, $orderType)
    {
        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];
        $repository = $this->getOrderRepository($this->orderConfigByCartAlias[$cartAlias]['orderClass']);
        $order = $repository->findOneBy([
            'id' => $id,
        ]);

        if (!$order) {
            throw $this->createNotFoundException();
        }

        $isHaveAccess = false;
        $renderArguments = [];

        if ($order instanceof UserOrder) {
            $isHaveAccess = $order->getUser()->getId() == $this->getUser()->getId();
        } elseif ($order instanceof GroupOrder) {
            /* @var $userHasGroup UserHasGroup*/
            $userHasGroup = $this->getDoctrine()->getRepository(UserHasGroup::class)
                    ->findOneBy([
                        'user' => $this->getUser(),
                        'userGroup' => $order->getUserGroup(),
                    ]);
            $isHaveAccess = !!$order->getUserGroup() ? $order->getUserGroup()->getId() == $userHasGroup->getUserGroup()->getId() : false;

            $orderData = [];

            /* @var $groupOrderHasProduct GroupOrderHasProduct */
            foreach ($order->getGroupOrderHasProducts() as $groupOrderHasProduct) {
                $user = $groupOrderHasProduct->getUserHasGroup()->getUser();
                $product = $groupOrderHasProduct->getProduct();

                $orderData[$user->getId()]['user'] = $user;
                $orderData[$user->getId()]['products'][] = [
                    'product' => $product,
                    'price' => $groupOrderHasProduct->getPrice(),
                    'quantity' => $groupOrderHasProduct->getQuantity(),
                ];
                if (empty($orderData[$user->getId()]['summaryData'])) {
                    $orderData[$user->getId()]['summaryData'] = [
                        'summaryQuantity' => 0,
                        'summaryCost' => 0
                    ];
                }

                $orderData[$user->getId()]['summaryData']['summaryQuantity'] += $groupOrderHasProduct->getQuantity();
                $orderData[$user->getId()]['summaryData']['summaryCost'] += $groupOrderHasProduct->getPrice();
            }

            $renderArguments['orderData'] = $orderData;
        }

        if (!$isHaveAccess) {
            throw $this->createAccessDeniedException();
        }

        $renderArguments[$orderType . 'Order'] = $order;
        $renderArguments['cartAlias'] = $cartAlias;

        return $this->render('@DeveloperLife/user/order/showOrder.html.twig', $renderArguments);
    }

    /**
     * Return order repository
     *
     * @param string $orderClass Repository class name
     * @return UserOrderRepository|GroupOrderRepository
     */
    private function getOrderRepository($orderClass)
    {
        return $this->getDoctrine()->getRepository($orderClass);
    }
}