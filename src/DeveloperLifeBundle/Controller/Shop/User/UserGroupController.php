<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 07.06.17
 * Time: 15:20
 */

namespace DeveloperLifeBundle\Controller\Shop\User;

use DeveloperLifeBundle\Entity\Shop\Group\JoinRequest;
use DeveloperLifeBundle\Entity\Shop\Group\UserGroup;
use DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup;
use DeveloperLifeBundle\Entity\Shop\User\User;
use DeveloperLifeBundle\Form\Shop\Group\UserGroupType;
use DeveloperLifeBundle\Repository\Shop\Group\JoinRequestRepository;
use DeveloperLifeBundle\Repository\Shop\Group\UserGroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserGroupController
 *
 * Handle user group
 *
 * @package DeveloperLifeBundle\Controller\Shop\User
 */
class UserGroupController extends Controller
{
    /**
     * User group list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /* @var $user User*/
        $user = $this->getUser();

        return $this->render('@DeveloperLife/user/userGroup/indexUserGroup.html.twig',[
            'ownerUserGroups' => $user->getUserGroup(),
            'memberUserHasGroups' => $user->getUserHasGroups()
        ]);
    }

    /**
     * New user group
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $userGroup = new UserGroup();

        $form = $this->createForm(UserGroupType::class, $userGroup, [
            'action' => $this->generateUrl('shop_profile_group_new'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userGroup->setOwner($this->getUser());

            $userHasGroup = new UserHasGroup();
            $userHasGroup->setUser($this->getUser());
            $userHasGroup->setUserGroup($userGroup);

            $em = $this->getDoctrine()->getManager();
            $em->persist($userGroup);
            $em->persist($userHasGroup);
            $em->flush();

            return $this->redirectToRoute('shop_profile_group_show',[
                'id' => $userGroup->getId(),
            ]);
        }

        return $this->render('@DeveloperLife/user/userGroup/userGroupForm.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edit user group
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id, Request $request)
    {
        $repository = $this->getUserGroupRepository();
        $userGroup = $repository->findOneBy([
            'id' => $id,
            'owner' => $this->getUser(),
        ]);

        if (!$userGroup) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(UserGroupType::class, $userGroup,[
            'action' => $this->generateUrl('shop_profile_group_edit', [
                'id' => $userGroup->getId(),
            ]),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userGroup);
            $em->flush();

            return $this->redirectToRoute('shop_profile_group_show', [
                'id' => $userGroup->getId(),
            ]);
        }

        return $this->render('@DeveloperLife/user/userGroup/userGroupForm.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Remove user group
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($id)
    {
        $repository = $this->getUserGroupRepository();
        $userGroup = $repository->findOneBy([
            'id' => $id,
            'owner' => $this->getUser(),
        ]);

        $em = $this->getDoctrine()->getManager();

        if (!$userGroup) {
            throw $this->createNotFoundException();
        }

        $em->remove($userGroup);
        $em->flush();

        return $this->redirectToRoute('shop_profile_group');
    }

    /**
     * Show user group
     *
     * @throws NotFoundHttpException
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $repository = $this->getUserGroupRepository();
        $userGroup = $repository->find($id);

        if (!$userGroup) {
            throw $this->createNotFoundException();
        }


        /* @var $userGroup UserGroup*/
        return $this->render('@DeveloperLife/user/userGroup/showUserGroup.html.twig',[
            'userGroup' => $userGroup
        ]);
    }

    /**
     * Groups available for join
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function joinListAction()
    {
        return $this->render('@DeveloperLife/user/userGroup/joinListUserGroup.html.twig', [
            'userGroups' => $this->getUserGroupRepository()->findJoinList($this->getUser()),
        ]);
    }

    /**
     * Join request to user group
     *
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @param integer $id
     * @param Request $request
     * @return JsonResponse
     */
    public function joinRequestAction($id, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createAccessDeniedException();
        }

        $userGroup = $this->getUserGroupRepository()->findJoinRequestGroup($this->getUser(), $id);

        if (!$userGroup) {
            throw $this->createNotFoundException();
        }

        if (!$this->getDoctrine()->getRepository(JoinRequest::class)->findOneBy([
            'user' => $this->getUser(),
            'userGroup' => $userGroup,
        ])) {
            $joinRequest = new JoinRequest();
            $joinRequest->setUser($this->getUser());
            $joinRequest->setUserGroup($userGroup);

            $em = $this->getDoctrine()->getManager();
            $em->persist($joinRequest);
            $em->flush();
        }

        $jsonResponse = new JsonResponse([
            'message' => $this->get('translator')->trans(
                'userGroup.message.join_request.success',
                [],
                'profile'
            ),
        ]);

        return $jsonResponse;
    }

    /**
     * Incoming join requests
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function joinRequestInAction()
    {
        return $this->render('@DeveloperLife/user/userGroup/joinRequestIn.html.twig', [
            'joinRequests' => $this->getJoinRequestRepository()->getJoinRequestIn($this->getUser()),
        ]);
    }

    /**
     * Render incoming join requests status
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function joinRequestInStatusAction()
    {
        return $this->render('@DeveloperLife/user/userGroup/partial/joinRequestStatus.html.twig',[
            'requestCount' => $this->getJoinRequestRepository()
                ->getCountJoinRequestIn($this->getUser()),
        ]);
    }

    /**
     * Confirm incoming join request
     *
     * @throws NotFoundHttpException
     * @throws AccessDeniedHttpException
     * @param $joinRequestId
     * @return JsonResponse
     */
    public function joinRequestInConfirmAction($joinRequestId)
    {
        $joinRequest = $this->getJoinRequestRepository()->find($joinRequestId);

        if (!$joinRequest) {
            throw $this->createNotFoundException();
        } elseif ($joinRequest->getUserGroup()->getOwner()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }

        $userHasGroup = new UserHasGroup();
        $userHasGroup->setUser($joinRequest->getUser());
        $userHasGroup->setUserGroup($joinRequest->getUserGroup());

        $em = $this->getDoctrine()->getManager();
        $em->remove($joinRequest);
        $em->persist($userHasGroup);
        $em->flush();

        $jsonResponse = new JsonResponse([
            'message' => $this->get('translator')->trans(
                'userGroup.message.join_request.confirmed',
                [],
                'profile'
            ),
        ]);

        return $jsonResponse;
    }

    /**
     * Return UserGroupRepository
     *
     * @return UserGroupRepository
     */
    private function getUserGroupRepository()
    {
        return $this->getDoctrine()->getRepository(UserGroup::class);
    }

    /**
     * Return JoinRequestRepository
     *
     * @return JoinRequestRepository
     */
    private function getJoinRequestRepository()
    {
        return $this->getDoctrine()->getRepository(JoinRequest::class);
    }
}