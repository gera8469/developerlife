<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 17:47
 */

namespace DeveloperLifeBundle\Controller\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use DeveloperLifeBundle\Helper\Cart\Base\AbstractCartHandler;
use DeveloperLifeBundle\Helper\Cart\Cart\Handler\GroupCartHandler;
use DeveloperLifeBundle\Helper\Cart\CartHandlerFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CartController
 *
 * Handle cart
 *
 * @package DeveloperLifeBundle\Controller\Shop\Shop
 */
class CartController extends Controller
{
    /**
     * Generate cart status
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCartStatusAction(Request $request)
    {
        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        $cart = CartHandlerFactory::getCartHandler(
            $cartAlias,
            $request->cookies->get($cartAlias),
            $this->get('service_container')
        );

        $response = $this->render('@DeveloperLife/shop/shop/cart/partial/cartStatus.html.twig',[
            'cartAlias' => $cartAlias,
            'cartData' => $cart->getCartData(),
        ]);

        return $response;
    }

    /**
     * Add product to cart
     *
     * @throws AccessDeniedHttpException
     * @param Request $request
     * @param integer $productId
     * @return JsonResponse
     */
    public function addProductAction(Request $request, $productId)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createAccessDeniedException();
        }

        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        return $this->handleCartAction('addProduct', $productId, $cartAlias, $request);
    }

    /**
     * Remove product from cart
     *
     * @throws AccessDeniedHttpException
     * @param Request $request
     * @param integer $productId
     * @return JsonResponse
     */
    public function removeProductAction(Request $request, $productId)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createAccessDeniedException();
        }

        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        return $this->handleCartAction('removeProduct', $productId, $cartAlias, $request);
    }

    /**
     * Remove all products by product id
     *
     * @throws AccessDeniedHttpException
     * @param Request $request
     * @param integer $productId
     * @return JsonResponse
     */
    public function removeAllProductsAction(Request $request, $productId)
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createAccessDeniedException();
        }

        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        return $this->handleCartAction('removeAllProducts', $productId, $cartAlias, $request);
    }

    /**
     * Clear cart
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function clearCartAction(Request $request)
    {
        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        if ($cartAlias == 'groupCart') {
            $groupCart = CartHandlerFactory::getCartHandler(
                $cartAlias,
                $request->cookies->get($cartAlias),
                $this->get('service_container')
            );
            $groupCart->clear();
        }

        $response = new RedirectResponse($this->generateUrl('shop_profile_cart'));
        $response->headers->clearCookie($cartAlias);

        return $response;
    }

    /**
     * Show cart
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction()
    {
        return $this->render('@DeveloperLife/shop/shop/cart/showCart.html.twig');
    }

    /**
     * Render partial cart template
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getShowCartPartialAction(Request $request)
    {
        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        $cart = CartHandlerFactory::getCartHandler(
            $cartAlias,
            $request->cookies->get($cartAlias),
            $this->get('service_container')
        );

        $cartData = $cart->getCartProducts();

        return $this->render('@DeveloperLife/shop/shop/cart/partial/showCart.html.twig', [
            'cartData' => $cartData,
            'cartAlias' => $cartAlias,
        ]);
    }

    /**
     * Check group cart update
     *
     * @throws AccessDeniedHttpException
     * @param Request $request
     * @return JsonResponse
     */
    public function checkUpdateAction(Request $request)
    {
        $cartAlias = AbstractCartHandler::getCartSettings($this->get('service_container'))['cartMode'];

        if ($cartAlias != 'groupCart') {
            throw $this->createAccessDeniedException();
        }

        /* @var  $groupCart GroupCartHandler */
        $groupCart = CartHandlerFactory::getCartHandler(
            $cartAlias,
            $request->cookies->get($cartAlias),
            $this->get('service_container')
        );

        $result = [
            'isUpdated' => false,
            'cartShowHtml' => ''
        ];

        if ($groupCart->isCartUpdated()) {
            $result['isUpdated'] = true;
            $result['cartShowHtml'] = $this->renderView('@DeveloperLife/shop/shop/cart/partial/showCart.html.twig',[
                'cartData' => $groupCart->getCartProducts(),
                'cartAlias' => $cartAlias,
            ]);
        }

        $response = new JsonResponse($result);

        return $response;
    }

    /**
     * Handle abstract cart action by action alias
     *
     * @throws NotFoundHttpException
     * @param string $action Action alias
     * @param integer $productId
     * @param string $cartAlias Cart alias (userCart|groupCart)
     * @param Request $request
     * @return JsonResponse
     */
    private function handleCartAction($action, $productId, $cartAlias, Request $request)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($productId);

        if (!$product) {
            throw $this->createNotFoundException();
        }

        $cart = CartHandlerFactory::getCartHandler(
            $cartAlias,
            $request->cookies->get($cartAlias),
            $this->get('service_container')
        );

        $cart->{$action}($product);
        $cartData = $cart->getJsonCartData();
        $cartStatusHtml = $this->renderView('@DeveloperLife/shop/shop/cart/partial/cartStatus.html.twig',[
            'cartAlias' => $cartAlias,
            'cartData' => $cart->getCartData(),
        ]);

        $response = new JsonResponse([
            'cartAlias' => $cartAlias,
            'cartData' => $cartData,
            'cartStatusHtml' => $cartStatusHtml,
        ]);
        $response->headers->setCookie(new Cookie($cartAlias, $cartData, time() + $cart::CART_LIFETIME));

        return $response;
    }
}