<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.05.17
 * Time: 21:28
 */

namespace DeveloperLifeBundle\Controller\Shop\Security;

use DeveloperLifeBundle\Entity\Shop\User\User;
use DeveloperLifeBundle\Form\Shop\User\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SecurityController
 *
 * @package DeveloperLifeBundle\Controller\Shop\Security
 */
class SecurityController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->redirectToRoute('shop_homepage');
        }

        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUserName = $authenticationUtils->getLastUsername();

        return $this->render('@DeveloperLife/shop/security/form/loginForm.html.twig', [
            'lastUserName' => $lastUserName,
            'error' => $error,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user,[
            'action' => $this->generateUrl('shop_user_register'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setRegistrationDate(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('shop_homepage');
        }

        $errors = $form->getErrors(true);

        return $this->render('@DeveloperLife/shop/security/form/registerForm.html.twig', [
            'form' => $form->createView(),
            'errors' => $errors,
        ]);
    }
}