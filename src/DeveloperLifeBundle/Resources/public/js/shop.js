/**
 * Created by vladimir on 04.06.17.
 */

$(function () {
    $('.zz-cart_add').bind('click', cart.addProduct);
    $('.zz-cart_plus_button').bind('click', cart.plusButtonHandler);
    $('.zz-cart_minus_button').bind('click', cart.minusButtonHandler);
    $('.zz-cart_remove_all').bind('click', cart.removeAllProducts);
    $('.zz-group_join_request').bind('click', group.joinRequest);
    $('.zz-group_join_confirm').bind('click', group.joinConfirm);
    $('.zz-cart_mode').bind('change', cart.switchCartMode)
    // cart.checkCartUpdate();
});

var group = {
    joinRequest: function (event) {
        var button = $(event.target);
        var userGroupId = button.data('user_group_id');
        $.ajax({
            url: Routing.generate('shop_profile_group_join_request', {
                'id': userGroupId
            }),
            method: 'post',
            success: function (response) {
                $('#zz-group_join_request_container_' + userGroupId).html(response.message);
            }
        });
    },
    joinConfirm: function (event) {
        console.log(event.target);
        var button = $(event.target);
        var joinRequestId = button.data('join_request_id');
        $.ajax({
            url: Routing.generate('shop_profile_group_join_request_confirm', {
                'joinRequestId': joinRequestId
            }),
            method: 'post',
            success: function (response) {
                $('#zz-group_join_request_container_' + joinRequestId).html(response.message);
            }
        });
    }
};

var cart = {
/*    checkCartUpdate: function () {
        $.ajax({
            url: Routing.generate('shop_user_cart_update'),
            method: 'post',
            async: true,
            success: function (response) {
                if (response.isUpdated){
                    $('#zz-cart_content_container').html(response.cartShowHtml);
                }
            }
        });
    },*/
    switchCartMode: function (event) {
        var radio = $(event.target);
        var cartModeAlias = radio.data('cart_mode_alias');
        if (cartModeAlias == 'groupCart') {
            $('#zz-cart_active_group_container').css('display', 'block');
        } else {
            $('#zz-cart_active_group_container').css('display', 'none');
        }
    },
    addProduct: function (event) {
        var button = $(event.target);
        $.ajax({
            url: Routing.generate('shop_user_cart_add', {
                'productId': button.data('product_id')
            }),
            method: 'post',
            success: function (response) {
                $('#zz-cart_status_container').html(response.cartStatusHtml);
            }
        });
    },
    plusButtonHandler: function (event) {
        var button = $(event.target);
        $.ajax({
            url: Routing.generate('shop_user_cart_add', {
                'productId': button.data('product_id')
            }),
            method: 'post',
            success: function (response) {
                $('#zz-cart_status_container').html(response.cartStatusHtml);
                var quantityInput = $('input', button.parent());
                quantityInput.val(+quantityInput.val() + 1);

                cart._changeSummaryDataStatus(response, button.parent().parent().parent());
            }
        });
    },
    minusButtonHandler: function (event) {
        var button = $(event.target);
        var quantityInput = $('input', button.parent());
        var changedQuantity = +quantityInput.val() - 1;
        if (changedQuantity >= 1) {
            $.ajax({
                url: Routing.generate('shop_user_cart_remove', {
                    'productId': button.data('product_id')
                }),
                method: 'post',
                success: function (response) {
                    $('#zz-cart_status_container').html(response.cartStatusHtml);
                    quantityInput.val(changedQuantity);

                    cart._changeSummaryDataStatus(response, button.parent().parent().parent());
                }
            });
        }
    },
    removeAllProducts: function (event) {
        var button = $(event.target);
        var quantityInput = $('input', button.parent());
        $.ajax({
            url: Routing.generate('shop_user_cart_remove_all', {
                'productId': button.data('product_id')
            }),
            method: 'post',
            success: function (response) {
                $('#zz-cart_status_container').html(response.cartStatusHtml);
                cart._changeSummaryDataStatus(response, button.parent().parent().parent());
                $('#zz-cart_product_container_' + button.data('product_id'), button.parent().parent().parent()).remove();


            }
        });
    },
    _changeSummaryDataStatus: function (response, parentObject) {
        var cartData = $.parseJSON(response.cartData);
        var summaryDataText = this._handleSummaryDataStatus(cartData, response.cartAlias);
        if (response.cartAlias == 'groupCart') {

        }
        $('.zz-summary_data_container', parentObject).html(summaryDataText);
    },
    _handleSummaryDataStatus: function (cartData, cartAlias) {
        var summaryDataText = '';
        if (cartAlias == 'userCart') {
            summaryDataText = this._generateSummaryDataStatus(cartData.summaryData.summaryQuantity,cartData. summaryData.summaryCost);
        } else if (cartAlias == 'groupCart') {
            var selfId = $('#zz-self_id').val();
            if (cartData.usersData[selfId]) {
                summaryDataText = this._generateSummaryDataStatus(cartData.usersData[selfId].summaryData.summaryQuantity,
                    cartData.usersData[selfId].summaryData.summaryCost);
            }

        }

        return summaryDataText;

    },
    _generateSummaryDataStatus: function (summaryQuantity, summaryCost) {
        return 'Количество блюд: ' + summaryQuantity + ' / Общая стоимость: ' + summaryCost;
    }
};