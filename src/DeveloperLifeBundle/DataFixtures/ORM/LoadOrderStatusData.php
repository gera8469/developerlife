<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.06.17
 * Time: 19:49
 */

namespace DeveloperLifeBundle\DataFixtures\ORM;

use DeveloperLifeBundle\DataFixtures\FixtureHelper;
use DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus;
use Doctrine\Common\Persistence\ObjectManager;

class LoadOrderStatusData extends FixtureHelper
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $orderStatusData = [
            [
                'name' => 'Новый',
                'alias' => 'new',
            ],
            [
                'name' => 'Обработанный',
                'alias' => 'handled',
            ],
            [
                'name' => 'Отмененный',
                'alias' => 'canceled',
            ],
        ];

        $this->handleFixtureData($manager, $orderStatusData, OrderStatus::class);

    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}