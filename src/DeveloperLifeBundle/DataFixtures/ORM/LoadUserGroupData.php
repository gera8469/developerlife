<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 07.06.17
 * Time: 17:51
 */

namespace DeveloperLifeBundle\DataFixtures\ORM;

use DeveloperLifeBundle\DataFixtures\FixtureHelper;
use DeveloperLifeBundle\Entity\Shop\Group\UserGroup;
use DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserGroupData extends FixtureHelper
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userGroup1 = new UserGroup();

        $userGroup1->setName('Some group');
        $userGroup1->setOwner($this->getReference('userVladimir'));

        $userHasGroup1 = new UserHasGroup();
        $userHasGroup1->setUser($this->getReference('userVladimir'));
        $userHasGroup1->setUserGroup($userGroup1);

        $userGroup2 = new UserGroup();

        $userGroup2->setName('Another group');
        $userGroup2->setOwner($this->getReference('userVladimir'));

        $userHasGroup2 = new UserHasGroup();
        $userHasGroup2->setUser($this->getReference('userVladimir'));
        $userHasGroup2->setUserGroup($userGroup2);

        $em = $this->container->get('doctrine')->getManager();
        $em->persist($userGroup1);
        $em->persist($userGroup2);
        $em->persist($userHasGroup1);
        $em->persist($userHasGroup2);

        $em->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}