<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 08.06.17
 * Time: 19:35
 */

namespace DeveloperLifeBundle\DataFixtures\ORM;

use DeveloperLifeBundle\DataFixtures\FixtureHelper;
use DeveloperLifeBundle\Entity\Shop\Group\Cart\CartMode;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadCartModeContent
 * 
 * @package DeveloperLifeBundle\DataFixtures\ORM
 */
class LoadCartModeContent extends FixtureHelper
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $cartModeData = [
            [
                'name' => 'Личная корзина',
                'alias' => 'userCart'
            ],
            [
                'name' => 'Групповая корзина',
                'alias' => 'groupCart',
            ]
        ];

        $this->handleFixtureData($manager, $cartModeData, CartMode::class);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 6;
    }
}