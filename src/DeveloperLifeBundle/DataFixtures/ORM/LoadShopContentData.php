<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 02.06.17
 * Time: 21:52
 */

namespace DeveloperLifeBundle\DataFixtures\ORM;

use DeveloperLifeBundle\DataFixtures\FixtureHelper;
use DeveloperLifeBundle\Entity\Shop\Shop\Category;
use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use Doctrine\Common\Persistence\ObjectManager;

class LoadShopContentData extends FixtureHelper
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $categoryData = [
            'cake' => [
                'name' => 'Десерты',
            ],
            'fish' => [
                'name' => 'Блюда из рыбы'
            ],
            'potato' => [
                'name' => 'Блюда из картофеля'
            ],
            'meat' => [
                'name' => 'Блюда из мяса'
            ],
        ];

        $categories = $this->handleFixtureData($manager, $categoryData, Category::class);

        $productData = [
            [
                'name' => 'Десерт 1',
                'price' => '100',
                'image' => 'demo/images/products/cake_001.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['cake'],
            ],
            [
                'name' => 'Десерт 2',
                'price' => '110',
                'image' => 'demo/images/products/cake_002.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['cake'],
            ],
            [
                'name' => 'Блюдо из рыбы 1',
                'price' => '115',
                'image' => 'demo/images/products/fish_001.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['fish'],
            ],
            [
                'name' => 'Блюдо из рыбы 2',
                'price' => '125',
                'image' => 'demo/images/products/fish_002.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['fish'],
            ],
            [
                'name' => 'Блюдо из мяса 1',
                'price' => '120',
                'image' => 'demo/images/products/meat_001.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['meat'],
            ],
            [
                'name' => 'Блюдо из мяса 2',
                'price' => '115',
                'image' => 'demo/images/products/meat_002.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['meat'],
            ],
            [
                'name' => 'Блюдо из мяса 3',
                'price' => '175',
                'image' => 'demo/images/products/meat_003.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['meat'],
            ],
            [
                'name' => 'Блюдо из мяса 4',
                'price' => '45',
                'image' => 'demo/images/products/meat_004.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['meat'],
            ],
            [
                'name' => 'Блюдо из мяса 5',
                'price' => '215',
                'image' => 'demo/images/products/meat_005.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['meat'],
            ],
            [
                'name' => 'Блюдо из картофеля 1',
                'price' => '25',
                'image' => 'demo/images/products/potato_001.jpg',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis ea iste magnam 
                        nobis optio pariatur ratione recusandae reiciendis repudiandae sequi. In ipsam laudantium nisi 
                        quis saepe vitae. Amet, culpa, est. Autem beatae eligendi excepturi fuga neque nihil nulla 
                        pariatur praesentium ratione vero! Aperiam blanditiis delectus dolores facere ipsam, maxime 
                        mollitia nisi odio praesentium quia, reiciendis reprehenderit rerum sapiente sunt voluptatum.',
                'category' => $categories['potato'],
            ],
        ];

        $this->handleFixtureData($manager, $productData, Product::class);
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}