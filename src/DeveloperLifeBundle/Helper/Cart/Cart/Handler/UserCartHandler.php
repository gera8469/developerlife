<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 19:05
 */

namespace DeveloperLifeBundle\Helper\Cart\Cart\Handler;

use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use DeveloperLifeBundle\Entity\Shop\User\Order\OrderStatus;
use DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder;
use DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct;
use DeveloperLifeBundle\Entity\Shop\User\User;
use DeveloperLifeBundle\Helper\Cart\Base\AbstractCartHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserCartHandler extends AbstractCartHandler
{

    public function __construct($jsonCartData, ContainerInterface $container)
    {
        parent::__construct($jsonCartData, $container);

        if (empty($this->cartData['products'])) {
            $this->cartData = [
                'products' => [],
                'summaryData' => [
                    'summaryCost' => 0,
                    'summaryQuantity' => 0,
                ]
            ];
        }


    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return array
     */
    public function addProduct(Product $product, $quantity = 1)
    {
        if (array_key_exists($product->getId(), $this->cartData['products'])) {
            $this->cartData['products'][$product->getId()]['quantity'] += (int)$quantity;
        } else {
            $this->cartData['products'][$product->getId()]['quantity'] = (int)$quantity;
        }

        $this->cartData['summaryData'] = $this->calculateSummaryData();

        return $this->cartData;
    }

    public function removeProduct(Product $product, $quantity = 1)
    {
        if (array_key_exists($product->getId(), $this->cartData['products'])) {
            if (($this->cartData['products'][$product->getId()]['quantity'] -= $quantity) < 1) {
                unset($this->cartData['products'][$product->getId()]);
            }
        }

        $this->cartData['summaryData'] = $this->calculateSummaryData();

        return $this->cartData;
    }

    public function removeAllProducts(Product $product)
    {
        if (array_key_exists($product->getId(), $this->cartData['products'])) {
            unset($this->cartData['products'][$product->getId()]);
        }

        $this->cartData['summaryData'] = $this->calculateSummaryData();

        return $this->cartData;
    }

    public function convertCartToOrder($orderObj = null)
    {
        if (is_null($orderObj) || !is_object($orderObj) || !($orderObj instanceof UserOrder)) {
            $orderObj = new UserOrder();
        }
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $doctrine = $this->container->get('doctrine');
        /* @var  $orderObj UserOrder */
        $orderObj->setCreationDate(new \DateTime());
        $orderObj->setNumber('uo' . $user->getId() . time());
        $orderObj->setOrderStatus($doctrine->getRepository(OrderStatus::class)->findOneBy([
            'alias' => 'new'
        ]));
        $orderObj->setUser($user);
        $em = $doctrine->getManager();
        $em->persist($orderObj);
        foreach ($this->getCartProducts()['products'] as $cartProduct) {
            /* @var  $product Product */
            $product = $cartProduct['product'];
            $userOrderHasProduct = new UserOrderHasProduct();
            $userOrderHasProduct->setProduct($product);
            $userOrderHasProduct->setUserOrder($orderObj);
            $userOrderHasProduct->setPrice($product->getPrice());
            $userOrderHasProduct->setQuantity($cartProduct['quantity']);
            $em->persist($userOrderHasProduct);
        }
        $em->flush();
    }


    public function getCartProducts()
    {
        $productObjects = $this->getProductObjectList($this->getProductIdList());
        $cartProductsData = $this->cartData['products'];

        $cartProducts = [];
        foreach ($productObjects as $product) {
            $cartProducts['products'][$product->getId()] = [
                'product' => $product,
                'quantity' => $cartProductsData[$product->getId()]['quantity']
            ];
        }

        $cartProducts['summaryData'] = $this->calculateSummaryData();

        return $cartProducts;
    }

    public function getJsonCartData()
    {
        return json_encode($this->cartData);
    }

    private function getProductIdList()
    {
        return array_keys($this->cartData['products']);
    }

    private function calculateSummaryData()
    {
        $productIdList = $this->getProductIdList();
        $productObjList = $this->getProductObjectList($productIdList);

        $summaryData = [
            'summaryCost' => 0,
            'summaryQuantity' => 0,
        ];

        /* @var $product Product */
        foreach ($productObjList as $product) {
            $productCartData = $this->cartData['products'][$product->getId()];
            $summaryData['summaryCost'] += $product->getPrice() * $productCartData['quantity'];
            $summaryData['summaryQuantity'] += $productCartData['quantity'];
        }

        return $summaryData;
    }

    public function isEmpty()
    {
        return empty($this->cartData['products']);
    }


}