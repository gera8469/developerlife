<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.06.17
 * Time: 19:28
 */

namespace DeveloperLifeBundle\Helper\Cart\Base;

use DeveloperLifeBundle\Entity\Shop\Group\UserGroup;
use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractCartHandler
 *
 * Implement base methods and declared signature methods which need for cart handling
 *
 * @package DeveloperLifeBundle\Helper\Cart\Base
 */
abstract class AbstractCartHandler
{
    /**
     * Cart data
     *
     * @var array
     */
    protected $cartData = [];

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Cart settings
     *
     * @var array
     */
    protected $cartSettings;


    /**
     * Cart cookie lifetime
     *
     * @var integer
     */
    const CART_LIFETIME = 365 * 24 * 3600 *1000;

    /**
     * AbstractCartHandler constructor.
     *
     * @param string $jsonCartData
     * @param ContainerInterface $container
     */
    public function __construct($jsonCartData, ContainerInterface $container){

        $this->cartData = json_decode($jsonCartData, true);
        $this->container = $container;
        $this->cartSettings = self::getCartSettings($this->container);
    }

    /**
     * Create cart handler object
     *
     * @param array $arguments
     * @return static
     */
    public static function createHandler(array $arguments){

        return new static($arguments['jsonCartData'], $arguments['container']);
    }

    /**
     * Return cart settings
     *
     * @param ContainerInterface $container
     * @return array|mixed
     */
    public static function getCartSettings(ContainerInterface $container)
    {
        $request = $container->get('request');
        $cartSettings = !empty($request->cookies->get('cartSettings'))
            ? json_decode($request->cookies->get('cartSettings'), true) : [
                'cartMode' => 'userCart',
                'activeGroup' => 0
            ];

        if (!$container->get('doctrine')->getRepository(UserGroup::class)->find($cartSettings['activeGroup'])) {
            $cartSettings['cartMode'] = 'userCart';
        }

        return $cartSettings;
    }

    /**
     * Add product to cart
     *
     * @param Product $product
     * @param int $quantity
     */
    abstract public function addProduct(Product $product, $quantity = 1);

    /**
     * Remove product from cart
     *
     * @param Product $product
     * @param int $quantity
     */
    abstract public function removeProduct(Product $product, $quantity = 1);

    /**
     * Remove all products from cart based on product entity
     *
     * @param Product $product
     */
    abstract public function removeAllProducts(Product $product);

    /**
     * Concert cart data to order and save to database
     *
     * @param null $orderObj
     */
    abstract public function convertCartToOrder($orderObj = null);

    /**
     *Clear all cart data
     */
    public function clear(){}

    /**
     * Check if cart empty
     *
     * @return mixed
     */
    abstract public function isEmpty();

    /**
     * Get cart product list
     *
     * @return array
     */
    abstract public function getCartProducts();

    /**
     * Get cart data in json format
     *
     * @return string
     */
    public function getJsonCartData(){

        return json_encode($this->getCartData());
    }

    /**
     * Get cart data in array format
     *
     * @return array
     */
    public function getCartData()
    {
        return $this->cartData;
    }

    /**
     * Find products by product id list
     *
     * @param array $productIdList
     * @return Product[]
     */
    protected function getProductObjectList(array $productIdList)
    {
        $repository = $this->container->get('doctrine')->getRepository(Product::class);

        return $repository->findByIdList($productIdList);
    }
}