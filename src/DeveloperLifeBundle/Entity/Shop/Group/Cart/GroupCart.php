<?php

namespace DeveloperLifeBundle\Entity\Shop\Group\Cart;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupCart
 */
class GroupCart
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $quantity;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return GroupCart
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup
     */
    private $userHasGroup;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Shop\Product
     */
    private $product;


    /**
     * Set userHasGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroup
     * @return GroupCart
     */
    public function setUserHasGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroup = null)
    {
        $this->userHasGroup = $userHasGroup;

        return $this;
    }

    /**
     * Get userHasGroup
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup 
     */
    public function getUserHasGroup()
    {
        return $this->userHasGroup;
    }

    /**
     * Set product
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Product $product
     * @return GroupCart
     */
    public function setProduct(\DeveloperLifeBundle\Entity\Shop\Shop\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Shop\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
