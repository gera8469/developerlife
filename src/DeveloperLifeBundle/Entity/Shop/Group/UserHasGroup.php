<?php

namespace DeveloperLifeBundle\Entity\Shop\Group;

use DeveloperLifeBundle\Entity\Shop\Group\Cart\GroupCart;
use Doctrine\ORM\Mapping as ORM;

/**
 * UserHasGroup
 */
class UserHasGroup
{
    /**
     * @var int
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupCarts;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\User
     */
    private $user;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Group\UserGroup
     */
    private $userGroup;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupCarts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupCarts
     *
     * @param GroupCart $groupCarts
     * @return UserHasGroup
     */
    public function addGroupCart(GroupCart $groupCarts)
    {
        $this->groupCarts[] = $groupCarts;

        return $this;
    }

    /**
     * Remove groupCarts
     *
     * @param GroupCart $groupCarts
     */
    public function removeGroupCart(GroupCart $groupCarts)
    {
        $this->groupCarts->removeElement($groupCarts);
    }

    /**
     * Get groupCarts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupCarts()
    {
        return $this->groupCarts;
    }

    /**
     * Set user
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\User $user
     * @return UserHasGroup
     */
    public function setUser(\DeveloperLifeBundle\Entity\Shop\User\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup
     * @return UserHasGroup
     */
    public function setUserGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup = null)
    {
        $this->userGroup = $userGroup;

        return $this;
    }

    /**
     * Get userGroup
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Group\UserGroup 
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrderHasProducts;


    /**
     * Add groupOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts
     * @return UserHasGroup
     */
    public function addGroupOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts)
    {
        $this->groupOrderHasProducts[] = $groupOrderHasProducts;

        return $this;
    }

    /**
     * Remove groupOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts
     */
    public function removeGroupOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts)
    {
        $this->groupOrderHasProducts->removeElement($groupOrderHasProducts);
    }

    /**
     * Get groupOrderHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupOrderHasProducts()
    {
        return $this->groupOrderHasProducts;
    }
}
