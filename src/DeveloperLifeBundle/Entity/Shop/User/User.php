<?php

namespace DeveloperLifeBundle\Entity\Shop\User;

use DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 */
class User implements
    UserInterface,
    \Serializable
{
    /**
     * @var integer
     */
    private $id;

    /**
     * Assert\NotBlank()
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var \DateTime
     */
    private $birthDate;

    /**
     * @var \DateTime
     */
    private $registrationDate;

    private $plainPassword;

    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return User
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set registrationDate
     *
     * @param \DateTime $registrationDate
     * @return User
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;

        return $this;
    }

    /**
     * Get registrationDate
     *
     * @return \DateTime 
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
        ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->email,
            $this->password
            ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }


    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $deliveryAddresses;


    /**
     * Add deliveryAddresses
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddresses
     * @return User
     */
    public function addDeliveryAddress(\DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddresses)
    {
        $this->deliveryAddresses[] = $deliveryAddresses;

        return $this;
    }

    /**
     * Remove deliveryAddresses
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddresses
     */
    public function removeDeliveryAddress(\DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress $deliveryAddresses)
    {
        $this->deliveryAddresses->removeElement($deliveryAddresses);
    }

    /**
     * Get deliveryAddresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDeliveryAddresses()
    {
        return $this->deliveryAddresses;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userOrders;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userHasGroups;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userGroup;


    /**
     * Add userOrders
     *
     * @param UserOrder $userOrders
     * @return User
     */
    public function addUserOrder(UserOrder $userOrders)
    {
        $this->userOrders[] = $userOrders;

        return $this;
    }

    /**
     * Remove userOrders
     *
     * @param UserOrder $userOrders
     */
    public function removeUserOrder(UserOrder $userOrders)
    {
        $this->userOrders->removeElement($userOrders);
    }

    /**
     * Get userOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserOrders()
    {
        return $this->userOrders;
    }

    /**
     * Add userHasGroups
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroups
     * @return User
     */
    public function addUserHasGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroups)
    {
        $this->userHasGroups[] = $userHasGroups;

        return $this;
    }

    /**
     * Remove userHasGroups
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroups
     */
    public function removeUserHasGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserHasGroup $userHasGroups)
    {
        $this->userHasGroups->removeElement($userHasGroups);
    }

    /**
     * Get userHasGroups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserHasGroups()
    {
        return $this->userHasGroups;
    }

    /**
     * Add userGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup
     * @return User
     */
    public function addUserGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup)
    {
        $this->userGroup[] = $userGroup;

        return $this;
    }

    /**
     * Remove userGroup
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup
     */
    public function removeUserGroup(\DeveloperLifeBundle\Entity\Shop\Group\UserGroup $userGroup)
    {
        $this->userGroup->removeElement($userGroup);
    }

    /**
     * Get userGroup
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserGroup()
    {
        return $this->userGroup;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $joinRequests;


    /**
     * Add joinRequest
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest
     *
     * @return User
     */
    public function addJoinRequest(\DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest)
    {
        $this->joinRequests[] = $joinRequest;

        return $this;
    }

    /**
     * Remove joinRequest
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest
     */
    public function removeJoinRequest(\DeveloperLifeBundle\Entity\Shop\Group\JoinRequest $joinRequest)
    {
        $this->joinRequests->removeElement($joinRequest);
    }

    /**
     * Get joinRequest
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJoinRequests()
    {
        return $this->joinRequests;
    }

    public function getStringRegistrationDate($format = 'd.m.y - H:i:s')
    {
        return !empty($this->registrationDate) ? $this->registrationDate->format($format) : '';
    }

    public function getStringBirthDate($format = 'd.m.y')
    {
        return !empty($this->birthDate) ? $this->birthDate->format($format) : '';
    }
}
