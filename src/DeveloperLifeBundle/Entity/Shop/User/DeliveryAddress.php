<?php

namespace DeveloperLifeBundle\Entity\Shop\User;

use DeveloperLifeBundle\Entity\Shop\User\Order\UserOrder;
use Doctrine\ORM\Mapping as ORM;

/**
 * DeliveryAddress
 */
class DeliveryAddress
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $house;

    /**
     * @var string
     */
    private $flat;

    /**
     * @var bool
     */
    private $isDeleted = false;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return DeliveryAddress
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return DeliveryAddress
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return DeliveryAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return DeliveryAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set house
     *
     * @param string $house
     * @return DeliveryAddress
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return string 
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set flat
     *
     * @param string $flat
     * @return DeliveryAddress
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;

        return $this;
    }

    /**
     * Get flat
     *
     * @return string 
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return DeliveryAddress
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
    /**
     * @var \DeveloperLifeBundle\Entity\Shop\User\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\User $user
     * @return DeliveryAddress
     */
    public function setUser(\DeveloperLifeBundle\Entity\Shop\User\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DeveloperLifeBundle\Entity\Shop\User\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userOrders;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add userOrders
     *
     * @param UserOrder $userOrders
     * @return DeliveryAddress
     */
    public function addUserOrder(UserOrder $userOrders)
    {
        $this->userOrders[] = $userOrders;

        return $this;
    }

    /**
     * Remove userOrders
     *
     * @param UserOrder $userOrders
     */
    public function removeUserOrder(UserOrder $userOrders)
    {
        $this->userOrders->removeElement($userOrders);
    }

    /**
     * Get userOrders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserOrders()
    {
        return $this->userOrders;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrders;


    /**
     * Add groupOrder
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrder
     *
     * @return DeliveryAddress
     */
    public function addGroupOrder(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrder)
    {
        $this->groupOrders[] = $groupOrder;

        return $this;
    }

    /**
     * Remove groupOrder
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrder
     */
    public function removeGroupOrder(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrder $groupOrder)
    {
        $this->groupOrders->removeElement($groupOrder);
    }

    /**
     * Get groupOrders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupOrders()
    {
        return $this->groupOrders;
    }
}
