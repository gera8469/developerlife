<?php

namespace DeveloperLifeBundle\Entity\Shop\Shop;

use DeveloperLifeBundle\EventListener\Doctrine\Helper\ImageContainingInterface;
use DeveloperLifeBundle\EventListener\Doctrine\Helper\ImageContainingTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Product
 */
class Product implements ImageContainingInterface
{
    use ImageContainingTrait;
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $image;

    /**
     * @var bool
     */
    private $isDeleted = false;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set descroption
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get descroption
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Product
     */
    public function setImage($image)
    {
        if (!empty($this->image)) {
            $this->oldImage = $this->image;
        }
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     * @return Product
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean 
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupCarts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $groupOrderHasProducts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userOrderHasProducts;

    /**
     * @var \DeveloperLifeBundle\Entity\Shop\Shop\Category
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupCarts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupOrderHasProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userOrderHasProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add groupCarts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Cart\GroupCart $groupCarts
     * @return Product
     */
    public function addGroupCart(\DeveloperLifeBundle\Entity\Shop\Group\Cart\GroupCart $groupCarts)
    {
        $this->groupCarts[] = $groupCarts;

        return $this;
    }

    /**
     * Remove groupCarts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Cart\GroupCart $groupCarts
     */
    public function removeGroupCart(\DeveloperLifeBundle\Entity\Shop\Group\Cart\GroupCart $groupCarts)
    {
        $this->groupCarts->removeElement($groupCarts);
    }

    /**
     * Get groupCarts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupCarts()
    {
        return $this->groupCarts;
    }

    /**
     * Add groupOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts
     * @return Product
     */
    public function addGroupOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts)
    {
        $this->groupOrderHasProducts[] = $groupOrderHasProducts;

        return $this;
    }

    /**
     * Remove groupOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts
     */
    public function removeGroupOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\Group\Order\GroupOrderHasProduct $groupOrderHasProducts)
    {
        $this->groupOrderHasProducts->removeElement($groupOrderHasProducts);
    }

    /**
     * Get groupOrderHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroupOrderHasProducts()
    {
        return $this->groupOrderHasProducts;
    }

    /**
     * Add userOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts
     * @return Product
     */
    public function addUserOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts)
    {
        $this->userOrderHasProducts[] = $userOrderHasProducts;

        return $this;
    }

    /**
     * Remove userOrderHasProducts
     *
     * @param \DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts
     */
    public function removeUserOrderHasProduct(\DeveloperLifeBundle\Entity\Shop\User\Order\UserOrderHasProduct $userOrderHasProducts)
    {
        $this->userOrderHasProducts->removeElement($userOrderHasProducts);
    }

    /**
     * Get userOrderHasProducts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserOrderHasProducts()
    {
        return $this->userOrderHasProducts;
    }

    /**
     * Set category
     *
     * @param \DeveloperLifeBundle\Entity\Shop\Shop\Category $category
     * @return Product
     */
    public function setCategory(\DeveloperLifeBundle\Entity\Shop\Shop\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DeveloperLifeBundle\Entity\Shop\Shop\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }




}
