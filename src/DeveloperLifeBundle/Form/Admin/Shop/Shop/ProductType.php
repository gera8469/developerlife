<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 01.06.17
 * Time: 20:37
 */

namespace DeveloperLifeBundle\Form\Admin\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Category;
use DeveloperLifeBundle\Entity\Shop\Shop\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductType
 *
 * @package DeveloperLifeBundle\Form\Admin\Shop\Shop
 */
class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'translation_domain' => 'admin',
                'label' => 'product.form.fieldLabel.name',
                'required' => false,
            ])
            ->add('price', TextType::class, [
                'translation_domain' => 'admin',
                'label' => 'product.form.fieldLabel.price',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'translation_domain' => 'admin',
                'label' => 'category.form.fieldLabel.description',
                'required' => false,
            ])
            ->add('uploadImage', FileType::class, [
                'translation_domain' => 'admin',
                'label' => 'product.form.fieldLabel.image',
//                'mapped' => false,
                'required' => false,
            ])
            ->add('category', EntityType::class,[
                'class' => Category::class,
                'choice_label' => 'name',
                'translation_domain' => 'admin',
                'label' => 'product.form.fieldLabel.category',
                'empty_data' => null,
                'empty_value' => 0,
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'translation_domain' => 'admin',
                'label' => 'product.form.fieldLabel.save',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'developerlifebundle_admin_shop_shop_product';
    }


}