<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 01.06.17
 * Time: 13:57
 */

namespace DeveloperLifeBundle\Form\Admin\Shop\Shop;

use DeveloperLifeBundle\Entity\Shop\Shop\Category;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CategoryType
 *
 * @package DeveloperLifeBundle\Form\Admin\Shop\Shop
 */
class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'translation_domain' => 'admin',
                'label' => 'category.form.fieldLabel.name',
            ])
            ->add('description', TextareaType::class, [
                'translation_domain' => 'admin',
                'label' => 'category.form.fieldLabel.description',
                'required' => false,
            ])
            ->add('parentCategory', EntityType::class,[
                'class' => Category::class,
                'choice_label' => 'name',
                'translation_domain' => 'admin',
                'label' => 'category.form.fieldLabel.parentCategory',
                'empty_data' => null,
                'empty_value' => 0,
                'required' => false,
                'query_builder' => function(EntityRepository $er ) use ($options) {
                    $categoryId = $options['data']->getId();
                    $query = null;
                    if (empty($categoryId)) {
                        $query = $er->createQueryBuilder('c')->select();
                    } else {
                        $query = $er->createQueryBuilder('c')
                            ->where('c.id <> ?1')
                            ->setParameter(1, $options['data']->getId());
                    }
                    return $query;
                },
            ])
            ->add('save', SubmitType::class, [
                'translation_domain' => 'admin',
                'label' => 'category.form.fieldLabel.save',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'developerlifebundle_admin_shop_shop_category';
    }


}