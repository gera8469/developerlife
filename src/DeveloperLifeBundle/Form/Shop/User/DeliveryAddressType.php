<?php

namespace DeveloperLifeBundle\Form\Shop\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryAddressType
 *
 * @package DeveloperLifeBundle\Form\Shop\User
 */
class DeliveryAddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.country',
            ])
            ->add('region', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.region',
            ])
            ->add('city', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.city',
            ])
            ->add('street', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.street',
            ])
            ->add('house', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.house',
            ])
            ->add('flat', TextType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.flat',
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'translation_domain' => 'profile',
                'label' => 'deliveryAddress.form.fieldLabel.submit',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DeveloperLifeBundle\Entity\Shop\User\DeliveryAddress'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'developerlifebundle_shop_user_deliveryaddress';
    }


}
