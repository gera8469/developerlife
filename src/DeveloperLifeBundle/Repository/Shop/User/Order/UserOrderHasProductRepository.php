<?php

namespace DeveloperLifeBundle\Repository\Shop\User\Order;

use Doctrine\ORM\EntityRepository;

/**
 * UserOrderHasProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserOrderHasProductRepository extends EntityRepository
{
}
