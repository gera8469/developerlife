<?php

namespace DeveloperLifeBundle\Repository\Shop\Group\Cart;

use DeveloperLifeBundle\Entity\Shop\Group\UserGroup;
use Doctrine\ORM\EntityRepository;

/**
 * GroupCartRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GroupCartRepository extends EntityRepository
{
    public function findGroupCartsByUserGroup(UserGroup $userGroup)
    {
        $queryBuilder = $this->createQueryBuilder('gc');
        $queryBuilder->select('gc')
            ->join('gc.userHasGroup', 'uhg', 'WITH', 'uhg.userGroup=:userGroup')
            ->setParameter(':userGroup', $userGroup);
        return $queryBuilder->getQuery()->getResult();
    }
}
