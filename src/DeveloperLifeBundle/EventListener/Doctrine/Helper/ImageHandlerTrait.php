<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 02.06.17
 * Time: 14:07
 */

namespace DeveloperLifeBundle\EventListener\Doctrine\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait ImageHandlerTrait
 *
 * Event listener helper. Implement methods which need for saving and removing images
 *
 * @package DeveloperLifeBundle\EventListener\Doctrine\Helper
 */
trait ImageHandlerTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Index based on entity name (Product => product). Need for extraction params from config file.
     *
     * @var
     */
    private $paramIndex;

    /**
     * Save new image
     *
     * @param ImageContainingInterface $entityObj
     */
    public function saveImage(ImageContainingInterface $entityObj)
    {
        $this->saveNewImage($entityObj);
    }

    /**
     * Update image
     *
     * @param ImageContainingInterface $entityObj
     */
    public function updateImage(ImageContainingInterface $entityObj)
    {
        if ($entityObj->isImageUpdated()) {
            $this->removeFile($entityObj->getOldImage());
            $this->saveNewImage($entityObj);
        }
    }

    /**
     * Handle image file saving
     *
     * @param ImageContainingInterface $entityObj
     */
    public function saveNewImage(ImageContainingInterface $entityObj)
    {
        $uploadImage = $entityObj->getUploadImage();
        $entityClass = get_class($entityObj);
        $serverPath = $this->generateServerPath($entityClass);
        $imageHandler = new \upload($uploadImage->getRealPath());
        $imageHandler->file_new_name_body = $this->getFileNameBody($entityObj->getImage());
        $imageHandler->image_convert = $uploadImage->guessExtension();
        $imageHandler->image_resize = true;
        $imageHandler->image_ratio_y = true;
        $imageHandler->image_x = $this->getImageWidth($entityClass);
        $imageHandler->process($serverPath);
    }

    /**
     * Remove image
     *
     * @param ImageContainingInterface $entityObj
     */
    public function removeImage(ImageContainingInterface $entityObj)
    {
        $this->removeFile($entityObj->getImage());
    }

    /**
     * Generate image file location path (full path)
     *
     * @param $className
     * @param string $additionalPath
     * @return string
     */
    private function generateServerPath($className, $additionalPath = '')
    {

        $paramIndex = $this->getParamIndex($className);
        $serverPath = $this->getGlobalPath() . '/' . $this->getRelativePath($paramIndex);
        if (!empty($additionalPath)) {
            $serverPath .= '/' . $additionalPath;
        }
        return $serverPath . '/';
    }

    /**
     * Global path
     *
     * @return string
     */
    private function getGlobalPath()
    {
        return $this->container->getParameter('image_config')['global_path'];
    }

    /**
     * Relative path
     *
     * @param $paramIndex
     * @return string
     */
    private function getRelativePath($paramIndex)
    {
        return $this->container->getParameter('image_config')[$paramIndex]['relative_path'];
    }

    /**
     * Image width
     *
     * @param $className
     * @return integer
     */
    private function getImageWidth($className)
    {
        return (int)$this->container->getParameter('image_config')[$this->getParamIndex($className)]['image_width'];
    }

    /**
     * Remove file
     *
     * @param $relativeFilePath
     */
    private function removeFile($relativeFilePath)
    {
        $file =$this->getGlobalPath() . '/' . $relativeFilePath;
        if (file_exists($file) && is_file($file)) {
            unlink($file);
        }

    }

    /**
     * Return file name body (full file name => somefile.jpg, file name body => somefile)
     *
     * @param $filePath
     * @return string
     */
    private function getFileNameBody($filePath)
    {
        $parts = explode('/', $filePath);
        $fullFileName = $parts[count($parts) - 1];

        return explode('.', $fullFileName)[0];
    }

    /**
     * Parse param index based on entity class name
     *
     * @param $className
     * @return string
     */
    private function getParamIndex($className)
    {
        if (empty($this->paramIndex)) {
            $parts = explode('\\', $className);
            $this->paramIndex = mb_strtolower($parts[count($parts) - 1]);
        }

        return $this->paramIndex;
    }
}