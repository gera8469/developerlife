<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 02.06.17
 * Time: 16:25
 */

namespace DeveloperLifeBundle\EventListener\Doctrine\Helper;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Interface ImageContainingInterface
 *
 * Declares the signature of methods required for working with images
 *
 * @package DeveloperLifeBundle\EventListener\Doctrine\Helper
 */
interface ImageContainingInterface
{
    /**
     * Return uploaded image
     *
     * @return UploadedFile
     */
    public function getUploadImage();

    /**
     * Set uploaded image
     *
     * @param UploadedFile $uploadImage
     */
    public function setUploadImage(UploadedFile $uploadImage);

    /**
     * Return entity`s id
     *
     * @return string|integer
     */
    public function getId();

    /**
     * Return old image
     *
     * @return string
     */
    public function getOldImage();

    /**
     * Return image file path
     *
     * @return string
     */
    public function getImage();

    /**
     * Check if image updated
     *
     * @return boolean
     */
    public function isImageUpdated();
}